const axios = require('axios');

function discoverRandomSession() {
    
    let url = `https://mercur-service-dev.cfapps.eu10.hana.ondemand.com/sessionSet`
    
    return axios.get(`${url}`, {
        params: {
            
        },
    }).then(response => {
        results = response.data.d.results
        if (results.length === 0) {
            return [{
                type: 'quickReplies',
                content: {
                    title: 'Sorry, but I could not find any results for your request :(',
                    buttons: [{ title: 'Start over', value: 'Start over' }],
                },
            }];
        }

        let lastIndex = results.length - 1
        let randomIndex = Math.floor((Math.random() * lastIndex) + 0);
        let card = results[randomIndex]

        const cards = [card].map(session => ({
            title: session.Title,
            subtitle: session.Description,
            imageUrl: `https://content.dm.ux.sap.com/content/dam/digitalassets1dx/customimages/events/sapandasug/loading-logo.png`,
            buttons: [
                {
                    type: 'web_url',
                    value: `https://www.google.de`,
                    title: 'View More',
                },
            ],
        }));

        return [
            {
                type: 'text',
                content: "Here's what I found for you!",
            },
            { type: 'carousel', content: cards },
        ];
    });
}

module.exports = discoverRandomSession;