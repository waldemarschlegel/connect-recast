const axios = require('axios');

function discoverSessions() {
    
    let url = `https://mercur-service-dev.cfapps.eu10.hana.ondemand.com/sessionSet`
    
    return axios.get(`${url}`, {
        params: {
            
        },
    }).then(response => {
        results = response.data.d.results
        if (results.length === 0) {
            return [{
                type: 'quickReplies',
                content: {
                    title: 'Sorry, but I could not find any results for your request :(',
                    buttons: [{ title: 'Start over', value: 'Start over' }],
                },
            }];
        }

        const cards = results.slice(0, 20).map(session => ({
            title: session.Title,
            subtitle: session.Description,
            imageUrl: `https://content.dm.ux.sap.com/content/dam/digitalassets1dx/customimages/events/sapandasug/loading-logo.png`,
            buttons: [
                {
                    type: 'web_url',
                    value: `https://www.google.de`,
                    title: 'View More',
                },
            ],
        }));

        return [
            {
                type: 'text',
                content: "Here's what I found for you!",
            },
            { type: 'carousel', content: cards },
        ];
    });
}

module.exports = discoverSessions;