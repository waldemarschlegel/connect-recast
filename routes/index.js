var express = require('express');
var discoverSessions = require('./discoverSessions.js')
var discoverRandomSession = require('./discoverRandomSession.js')
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

// Recast will send a post request to /errors to notify important errors
// described in a json body
router.post('/errors', (req, res) => {
  console.error(req.body);
  res.sendStatus(200);
});

router.post('/discover-sessions', (req, res) => {
  console.log('[POST] /discover-sessions');
  console.log('Request Body: ' + JSON.stringify(req.body.conversation));
  
  return discoverSessions()
    .then((carouselle) => res.json({
      replies: carouselle,
    }))
    .catch((err) => console.error('sessionsApi::discoverSessions error: ', err))
})

router.post('/discover-random-session', (req, res) => {
  console.log('[POST] /discover-sessions');
  console.log('Request Body: ' + JSON.stringify(req.body.conversation));

  return discoverRandomSession()
    .then((carouselle) => res.json({
      replies: carouselle,
    }))
    .catch((err) => console.error('sessionsApi::discoverSessions error: ', err))
})


module.exports = router;
